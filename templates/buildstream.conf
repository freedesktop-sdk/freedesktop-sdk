<%!
    import os

    REMOTE_CAS = "cache.freedesktop-sdk.io"
    if os.cpu_count() > 16:
        BUILD_TASKS = 4
        MAX_JOBS = os.cpu_count() // BUILD_TASKS
    else:
        BUILD_TASKS = 2
        MAX_JOBS = os.cpu_count()
    LOCAL_CAS = os.environ.get("LOCAL_CAS")
    # CACHE_PUSH has extra quotes in .gitlab-ci.yml due to
    #implementation details
    CACHE_PUSH = os.environ.get("CACHE_PUSH", "true").strip("'")
%>

build:
  max-jobs: ${MAX_JOBS}
  retry-failed: True

## More logs and better timestamps for non-interactive output
logging:
  message-format: '[%{elapsed}][%{key}][%{element}] %{action} %{message}'
  error-lines: 80
  throttle-ui-updates: False

scheduler:
  builders: ${BUILD_TASKS}
  network-retries: 4
  on-error: continue
  fetchers: 4  # Deprioritize fetching in favor to building

## Because we want tracking to succeed even if origin disappears
track:
  source: all

artifacts:
  override-project-caches: true
  servers:
  - url: "https://${REMOTE_CAS}:11004"
    push: ${CACHE_PUSH|n}
    connection-config:
      keepalive-time: 60
    auth:
      access-token: ~/.config/freedesktop-sdk.token

source-caches:
  override-project-caches: true
  servers:
  - url: https://${REMOTE_CAS}:11004
    push: ${CACHE_PUSH}
    connection-config:
      keepalive-time: 60
    auth:
      access-token: ~/.config/freedesktop-sdk.token

% if LOCAL_CAS == "true":
cache:
  storage-service:
    url: unix:/run/casd/casd.sock
    connection-config:
      keepalive-time: 60
% endif
