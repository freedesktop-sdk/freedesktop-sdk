#!/bin/bash

# flat-manager tokens to upload the releases
if ! grep -E "^BRANCH=.*beta$" Makefile
then
  export RELEASE_CHANNEL=stable
else
  export RELEASE_CHANNEL=beta
fi

if [ -n "$CI_COMMIT_TAG" ] && [ -n "$FLATHUB_REPO_TOKEN" ]; then
    export RELEASES_SERVER_ADDRESS=https://hub.flathub.org/
    export REPO_TOKEN="${FLATHUB_REPO_TOKEN}"
    case "${CI_COMMIT_TAG}" in
      *rc*) ;&
      *beta*)
        test "$RELEASE_CHANNEL" = "beta"
        ;;
        *)
        test "$RELEASE_CHANNEL" = "stable"
        # Check we have enabled stable ABI before we do any stable release
        [ "${STABLE_ABI}" = "true" ]
        ;;
    esac
elif [ -n "$RELEASES_REPO_TOKEN" ]; then
    export REPO_TOKEN=$RELEASES_REPO_TOKEN
    export RELEASES_SERVER_ADDRESS=https://releases.freedesktop-sdk.io/
    # We always use "stable" here. This is all beta on this server.
    export RELEASE_CHANNEL=stable
fi

if [ -n "$CI_COMMIT_TAG" ] && [ -n "$SNAPCRAFT_LOGIN_FILE" ]; then
    case "${CI_COMMIT_TAG}" in
      *beta*)
        export SNAP_RELEASE=beta
        export SNAP_GRADE=devel
        ;;
      *rc*)
        export SNAP_RELEASE=candidate
        export SNAP_GRADE=stable
        ;;
      *)
        export SNAP_RELEASE=stable
        export SNAP_GRADE=stable
        ;;
    esac
elif [ -n "$SNAPCRAFT_LOGIN_FILE" ]; then
    export SNAP_RELEASE=edge
    export SNAP_GRADE=devel
else
    export SNAP_GRADE=devel
fi

if [ -n "$CI_COMMIT_TAG" ]; then
    case "${CI_COMMIT_TAG}" in
        *rc*) ;&
        *beta*)
            export DOCKER_VERSION="${RUNTIME_VERSION}-beta"
            ;;
        *)
            export DOCKER_VERSION="${RUNTIME_VERSION}"
            ;;
    esac
else
    export DOCKER_VERSION="${RUNTIME_VERSION}-devel"
fi
